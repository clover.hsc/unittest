import { LoginResponse, UserList } from './../response';
import { IAccount } from '../constant';
import { TestBed } from '@angular/core/testing';

import { of, Observable } from 'rxjs';

// http testing module and  mocking controller
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { LoginService } from './login.service';

describe('LoginService', () => {
  // For http api client.
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: LoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(LoginService);

    // inject the http service.
    httpClient = TestBed.inject(HttpClient);

    // inject the test controller for each test
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return "Success" if success by HttpTestingController', () => {
    service
      .login({ email: 'admin@mail', password: 'admin' })
      .subscribe((res: LoginResponse) => {
        expect(res.message).toBe('Success');
      });

    const req = httpTestingController.expectOne('/login');
    expect(req.request.method).toEqual('POST');
    req.flush({ message: 'Success' });
    httpTestingController.verify();
  });
});

describe('Fake LoginService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: FakeLoginService;

  beforeEach(() => {
    service = new FakeLoginService(httpClient);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provider: HttpClient, useValue: httpClient },
        { provider: LoginService, useValue: service },
      ],
    });

    // inject the http service.
    // httpClient = TestBed.inject(HttpClient);

    // inject the test controller for each test
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be return "Success" if success by Fake Service', () => {
    service
      .login({ email: 'clover@mail', password: '246810' })
      .subscribe((res: LoginResponse) => {
        console.log(res);
        expect(res.message).toBe('Success');
      });
  });
});

/**
 * Jasmine Create Spy Object
 */
describe('LoginService SpyObj', () => {
  // For http api client.
  let serviceSpy: jasmine.SpyObj<LoginService>;

  beforeEach(() => {
    serviceSpy = jasmine.createSpyObj('LoginService', ['login']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: LoginService, useValue: serviceSpy }],
    });
  });

  it('should be created', () => {
    expect(serviceSpy).toBeTruthy();
  });

  it('should be return "Success" if success by Jasmine SpyObj', () => {
    const resp: Observable<LoginResponse> = of({ message: 'Success' });
    serviceSpy.login.and.returnValue(resp);
    serviceSpy
      .login({ email: 'admin@mail', password: 'admin' })
      .subscribe((res: LoginResponse) => {
        expect(res.message).toBe('Success');
      });
  });
});

/**
 * Jasmine Jasmine SpyObj
 */
describe('LoginService SpyOn', () => {
  let service: LoginService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LoginService, HttpClient],
    });

    service = TestBed.inject(LoginService);
    spyOn(service, 'login').and.returnValue(of({ message: 'Success' }));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be return "Success" if success by Jasmine SpyObj', () => {
    service
      .login({ email: 'admin@mail', password: 'admin' })
      .subscribe((res: LoginResponse) => {
        expect(res.message).toBe('Success');
      });
  });
});

/**
 * Fake server
 */
class FakeLoginService extends LoginService {
  login(account: IAccount): Observable<LoginResponse> {
    return of({ message: 'Success' });
  }
}
