import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserList, LoginResponse } from '../response';
import { IAccount } from '../constant';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login(account: IAccount): Observable<LoginResponse> {
    const url = '/login';
    return this.http.post<LoginResponse>(url, account);
  }
}
