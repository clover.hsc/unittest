import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { respUserList } from '../response';

@Injectable({
  providedIn: 'root',
})
export class TableService {
  constructor(private http: HttpClient) {}

  getList(): Observable<respUserList> {
    const url = '/list';
    return this.http.get<respUserList>(url);
  }
}
