import { LoginResponse } from './../response';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { IAccount } from '../constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  unauthorized = '';
  loginForm!: FormGroup;

  emailErrorMsg = 'Must be email format';
  passwordErrorMsg = 'Must more then 6 characters';

  constructor(
    private fb: FormBuilder,
    private loginServ: LoginService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  submit(): void {
    const account: IAccount = { email: '', password: '' };
    account.email = this.loginForm.value.email;
    account.password = this.loginForm.value.password;
    this.loginServ.login(account).subscribe(
      (res: LoginResponse) => {
        // console.log(res);
        this.unauthorized = '';
        this.route.navigateByUrl('/table');
      },
      (error) => {
        // console.log(error);
        this.unauthorized = 'Unauthorized Access';
      }
    );
  }
}
