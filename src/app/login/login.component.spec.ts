import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import {
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Router, RouterModule } from '@angular/router';
import { Location } from '@angular/common';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable, of, throwError } from 'rxjs';

import { LoginResponse } from './../response';
import { LoginService } from './../service/login.service';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';

import { LoginComponent } from './login.component';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(
  (key) => antDesignIcons[key]
);

describe('LoginComponent [SpyOn, nativeElement]', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginServSpy: LoginService;
  let routeSpy: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        NzTypographyModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
      ],
      declarations: [LoginComponent],
      providers: [{ provide: NZ_ICONS, useValue: icons }],
    }).compileComponents();

    loginServSpy = TestBed.inject(LoginService);
    routeSpy = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // add x before 'it' to ignore the testing
  xit('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create service', inject([LoginService], (service: LoginService) =>
    expect(service).toBeTruthy()
  ));

  it('If Password is invalid need to show "Must more than 6 characters" message', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '1234',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const errorMsg: HTMLElement =
      fixture.nativeElement.querySelector('div .ng-trigger');
    expect(errorMsg.innerText).toBe(component.passwordErrorMsg);
  });

  it('If Password is invalid , The "Login In" should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '1234',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const loginBTN: HTMLButtonElement =
      fixture.nativeElement.querySelector('button');
    expect(loginBTN.disabled).toBe(true);
  });

  it('If email is invalid need to show "Must be email format" message', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test',
      password: '123456',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const errorMsg: HTMLElement =
      fixture.nativeElement.querySelector('div .ng-trigger');
    expect(errorMsg.innerText).toBe(component.emailErrorMsg);
  });

  it('If email is invalid , The "Login In" should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test',
      password: '123456',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const loginBTN: HTMLButtonElement =
      fixture.nativeElement.querySelector('button');
    expect(loginBTN.disabled).toBe(true);
  });

  it('If Email and Password are invalid , The "Login In" should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test',
      password: '1234',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const loginBTN: HTMLButtonElement =
      fixture.nativeElement.querySelector('button');
    expect(loginBTN.disabled).toBe(true);
  });

  it('If Password and Email are valid , The "Login In" should be enabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '123456',
    });

    fixture.detectChanges();

    // Use nativeElement api.
    const loginBTN: HTMLButtonElement =
      fixture.nativeElement.querySelector('button');
    expect(loginBTN.disabled).toBe(false);
  });

  it('If account is valid, Get "Success" from login api', () => {
    component.loginForm.markAllAsTouched();
    const account = {
      email: 'test@mail.com',
      password: '123456',
    };
    fixture.detectChanges();

    spyOn(loginServSpy, 'login').and.returnValue(of({ message: 'Success' }));

    component.loginForm.patchValue(account);
    loginServSpy.login(account).subscribe((res: LoginResponse) => {
      expect(res.message).toBe('Success');
    });
  });

  it('If account is valid, Should access to "/table" url by routing', fakeAsync(() => {
    component.loginForm.markAllAsTouched();
    const account = {
      email: 'test@mail.com',
      password: '123456',
    };

    component.loginForm.patchValue(account);

    fixture.detectChanges();

    spyOn(loginServSpy, 'login').and.returnValue(of({ message: 'Success' }));
    const nav = spyOn(routeSpy, 'navigateByUrl');

    // Login button click behavior
    const LoginBtn = fixture.nativeElement.querySelector('button');
    LoginBtn.click();

    expect(nav).toHaveBeenCalledWith('/table');
  }));
});

/**
 * Use Jasmine SpyObj
 */
describe('LoginComponent [SpyObj DebugElment]', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let serviceSpy = jasmine.createSpyObj('LoginService', ['login']);
  let routeSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        ReactiveFormsModule,
        HttpClientTestingModule,
        NzFormModule,
        NzInputModule,
        NzTypographyModule,
        BrowserAnimationsModule,
      ],
      declarations: [LoginComponent],
      providers: [
        { provide: NZ_ICONS, useValue: icons },
        { provide: Router, useValue: routeSpy },
        { provide: LoginService, useValue: serviceSpy },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**
   * Debugelement.
   * Due to if run unit test in server side. There may not exist any browser to use 'querySelector'.
   * DebugElement is provide by Angular to help use DOM api in server side.
   */
  it('If email is invalid need to show "Must be email format" message', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test',
      password: '123456',
    });
    fixture.detectChanges();

    let emailErrMsgEl: DebugElement = fixture.debugElement.query(
      By.css('div .ng-trigger')
    );

    expect(emailErrMsgEl.nativeElement.innerText).toBe(component.emailErrorMsg);
  });

  it('If email is invalid. The "Log in" button should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test',
      password: '123456',
    });

    fixture.detectChanges();

    let loginBTN: DebugElement = fixture.debugElement.query(By.css('button'));

    expect(loginBTN.nativeElement.disabled).toBe(true);
  });

  it('If password is invalid need to show "Must more than 6 characters" message', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '1234',
    });
    fixture.detectChanges();

    let passwdErrMsgEl = fixture.debugElement.query(By.css('div.ng-trigger'));

    expect(passwdErrMsgEl.nativeElement.innerText).toBe(
      component.passwordErrorMsg
    );
  });

  it('If password is invalid. The "Log in" button should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '1234',
    });
    fixture.detectChanges();

    let loginBTN: DebugElement = fixture.debugElement.query(By.css('button'));

    expect(loginBTN.nativeElement.disabled).toBe(true);
  });

  it('If email and password are invalid. The "Log in" button should be disabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@',
      password: '1234',
    });
    fixture.detectChanges();

    let loginBTN: DebugElement = fixture.debugElement.query(By.css('button'));

    expect(loginBTN.nativeElement.disabled).toBe(true);
  });

  it('If email and password are valid. The "Log in" button should be Enabled', () => {
    component.loginForm.markAllAsTouched();
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: '123456',
    });
    fixture.detectChanges();

    let loginBTN: DebugElement = fixture.debugElement.query(By.css('button'));

    expect(loginBTN.nativeElement.disabled).toBe(false);
  });

  it('If account is invalid. The "Unauthorized Access" should be exposed', () => {
    component.loginForm.patchValue({
      email: 'test@mail.com',
      password: 'testtest',
    });

    // use throwError
    const resp: Observable<LoginResponse> = throwError({
      status: 404,
      message: `Unauthorized Access - ${component.loginForm.value.email}`,
    });

    // Spy the response attribute
    serviceSpy.login.and.returnValue(resp);

    // Submit function
    component.submit();
    fixture.detectChanges();

    const SignInMsgEl: DebugElement = fixture.debugElement.query(
      By.css('#SignInFailedMSG')
    );

    expect(SignInMsgEl.nativeElement.innerText).toBe(`Unauthorized Access`);
  });

  it('If account is valid. The response message should be "Success"', () => {
    const account = {
      email: 'test@mail.com',
      password: '12345',
    };

    const resp: Observable<LoginResponse> = of({
      message: `Success`,
    });

    // Spy the response data
    serviceSpy.login.and.returnValue(resp);

    // Login button click behavior
    serviceSpy.login(account).subscribe((res: LoginResponse) => {
      expect(res.message).toBe(`Success`);
    });
  });

  it('If account is valid. Should redirect to "/table" URL', fakeAsync(() => {
    component.loginForm.markAllAsTouched();
    const account = {
      email: 'test@mail.com',
      password: '123456',
    };

    component.loginForm.patchValue(account);
    fixture.detectChanges();

    const resp: Observable<LoginResponse> = of({
      message: `Success`,
    });

    // Spy the response data
    serviceSpy.login.and.returnValue(resp);

    // Login button click behavior
    const LoginBtn: DebugElement = fixture.debugElement.query(By.css('button'));
    LoginBtn.nativeElement.click();

    expect(routeSpy.navigateByUrl).toHaveBeenCalledWith('/table');
  }));
});
