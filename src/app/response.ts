import * as faker from 'faker';

export interface UserList {
  FirstName: string;
  LastName: string;
  Age: number;
  Email: string;
}

export interface respUserList {
  list: UserList[];
}

export const tableList: Array<UserList> = Array();

for (let i = 1; i <= 10; i++) {
  let user = <UserList>{};
  user.FirstName = faker.name.firstName();
  user.LastName = faker.name.lastName();
  user.Age = Math.floor(Math.random() * 100);
  user.Email = faker.internet.email();
  tableList.push(user);
}

export interface LoginResponse {
  message: string;
}
