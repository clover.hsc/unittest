import { rest } from 'msw';
import { tableList } from './response';
import { IAccount } from './constant';

export const handlers = [
  rest.post('/login', (req, rest, ctx) => {
    let { email, password } = <IAccount>req.body;
    if (email === 'admin@mail.com' && password === '123456') {
      return rest(ctx.status(200), ctx.json({ message: 'Success' }));
    } else {
      return rest(
        ctx.status(401),
        ctx.json({ message: `Unauthorized Access - ${email}` })
      );
    }
  }),
  rest.get('/list', (req, rest, ctx) => {
    return rest(ctx.status(200), ctx.json({ list: tableList }));
    // return rest(ctx.status(200), ctx.json({ list: [] }));
  }),
];
