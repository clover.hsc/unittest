import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user-list-routing.module';
import { TableComponent } from './table/table.component';
import { NzTableModule } from 'ng-zorro-antd/table';

@NgModule({
  declarations: [TableComponent],
  imports: [CommonModule, UserListRoutingModule, NzTableModule],
})
export class UserListModule {}
