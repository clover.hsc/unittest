import { TableService } from './../../service/table.service';
import { Component, OnInit } from '@angular/core';
import { UserList, respUserList } from 'src/app/response';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  tableData!: UserList[];

  constructor(private tableServ: TableService) {}

  ngOnInit(): void {
    this.GetTable();
  }

  GetTable(): void {
    this.tableServ.getList().subscribe((data: respUserList) => {
      data ? (this.tableData = data.list) : (this.tableData = []);
    });
  }
}
