import { respUserList } from './../../response';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { DebugElement } from '@angular/core';

import { TableComponent } from './table.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { TableService } from '../../service/table.service';

import * as faker from 'faker';

describe('TableComponent [SpyOn]', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let tbServ: TableService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzTableModule, HttpClientTestingModule],
      declarations: [TableComponent],
    }).compileComponents();

    tbServ = TestBed.inject(TableService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('It should exist "First Name" column', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const tbHeaderEl: DebugElement = fixture.debugElement.query(
      By.css('thead > tr > th')
    );

    expect(tbHeaderEl.nativeElement.textContent.trim()).toBe('First Name');
  });

  it('It should exist "Last Name" column', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const tbHeaderEl: DebugElement = fixture.debugElement.query(
      By.css('thead > tr > th:nth-of-type(2)')
    );

    expect(tbHeaderEl.nativeElement.textContent.trim()).toBe('Last Name');
  });

  it('It should exist "Age" column', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const tbHeaderEl: DebugElement = fixture.debugElement.query(
      By.css('thead > tr > th:nth-of-type(3)')
    );

    expect(tbHeaderEl.nativeElement.textContent.trim()).toBe('Age');
  });

  it('It should exist "Email" column', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const tbHeaderEl: DebugElement = fixture.debugElement.query(
      By.css('thead > tr > th:nth-of-type(4)')
    );

    expect(tbHeaderEl.nativeElement.textContent.trim()).toBe('Email');
  });

  it('It should exist "Pagination" link if exist list data', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const paginationBtn: DebugElement = fixture.debugElement.query(
      By.css('li > a')
    );

    expect(paginationBtn).toBeTruthy();
  });

  it('The pagination link should be "1" if exist list data', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const paginationBtn: DebugElement = fixture.debugElement.query(
      By.css('li > a')
    );

    expect(paginationBtn.nativeElement.innerText).toBe('1');
  });

  it('It should exist "Move back" button if exist list data', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const btn: DebugElement = fixture.debugElement.query(
      By.css('li[class^="ant-pagination-prev"] > button')
    );

    expect(btn).toBeTruthy();
  });

  it('It should exist "Move forward" button if exist list data', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    // Query 1's <th>
    const btn: DebugElement = fixture.debugElement.query(
      By.css('li[class^="ant-pagination-next"] > button')
    );

    expect(btn).toBeTruthy();
  });

  it('If get the non-empty list, the empty icon should be hidden.', () => {
    const tbList: Observable<respUserList> = of({
      list: [
        {
          FirstName: faker.name.firstName(),
          LastName: faker.name.lastName(),
          Age: Math.floor(Math.random() * 100),
          Email: faker.internet.email(),
        },
      ],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    const emptyIcon: DebugElement = fixture.debugElement.query(By.css('img'));

    expect(emptyIcon).toBeNull();
  });

  it('If get the empty list, should expose empty icon', () => {
    const tbList: Observable<respUserList> = of({
      list: [],
    });
    spyOn(tbServ, 'getList').and.returnValue(tbList);

    component.GetTable();

    fixture.detectChanges();

    const emptyIcon: DebugElement = fixture.debugElement.query(By.css('img'));

    expect(emptyIcon.attributes.src).toBe('assets/nodata.svg');
  });
});
